package com.kyp.test;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
@SpringBootApplication(scanBasePackages = "com.kyp.test")
public class KypApplication {

  public static void main(String[] args) {
    try {
      log.info("Starting KYP");
      SpringApplication app = new SpringApplication(KypApplication.class);
      app.run(args);
    } catch (Exception e) {
      log.error("Cannot start application", e);
    }
  }
}
