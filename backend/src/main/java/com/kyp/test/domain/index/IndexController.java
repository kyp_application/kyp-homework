package com.kyp.test.domain.index;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api")
class IndexController {

  @GetMapping(value = "/")
  public ResponseEntity<String> hi() {
    return ResponseEntity.ok("Hi");
  }
}